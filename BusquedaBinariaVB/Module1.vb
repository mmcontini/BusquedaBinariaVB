﻿Module Module1

    Sub Main()

        ' Se crea array ordenado
        Dim arr() As Integer = New Integer() {3, 12, 32, 34, 45, 90}

        ' x es el valor a buscar
        ' pos es el indice si es encontrado o -1
        Dim x, pos As Integer

        ' Lectura de consola
        Console.Write("Entre valor a buscar en array:")
        x = Integer.Parse(Console.ReadLine())

        ' Se obtiene índice o -1
        pos = busquedaBinaria(arr, x, 0, arr.Length - 1)

        ' Se muestra mensaje correspondiente
        If (pos <> -1) Then
            Console.WriteLine("Elemento hallado en posición {0}.", pos)
        Else : Console.WriteLine("Elemento no encontrado!")
        End If

        ' Se espera a que se pulse una tecla
        Console.ReadLine()

    End Sub

    ' Lógica de búsqueda binaria
    ' Retorna índice o -1
    Function busquedaBinaria(ByVal dataset() As Integer, ByVal x As Integer, ByVal limInf As Integer, ByVal limSup As Integer) As Integer

        ' Medio
        Dim medio As Integer

        While limSup >= limInf
            medio = (limInf + limSup) / 2
            If x = dataset(medio) Then
                Return medio
            ElseIf x < dataset(medio) Then
                limSup = medio - 1
            ElseIf x > dataset(medio) Then
                limInf = medio + 1
            End If
        End While
        Return -1
    End Function
End Module